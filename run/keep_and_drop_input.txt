# AK4 jets
keep Jet_*

# objects
dropmatch n?Tau_.+
dropmatch n?SoftActivity.+
dropmatch n?FsrPhoton_.+
dropmatch n?IsoTrack_.+
dropmatch n?btagWeight_.+

# HLT
drop HLT_*

# L1/TrigObj
dropmatch n?L1_.+
dropmatch n?TrigObj_.+
