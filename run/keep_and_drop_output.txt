# this is a comment
dropmatch n?[A-Z][a-zA-Z0-9]{1,}.+
drop btagWeight_*

# objects
drop Tau*

# event info
keep run
keep luminosityBlock
keep event
keep PV_npvs

# triggers
drop HLT*

# genweight
keep genWeight
keep LHE_Vpt
